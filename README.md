MAIIA-CV Package
==============================

ML tools for applying computer vision algorithms for informal settlements delineation

Project Organization
------------

    │
    │
    ├── bin/               <- Scripts (run training and inference from terminal)
    |
    ├── gim_cv/      <- Python module with source code of this project.
    │   ├── interfaces/    <- submodules for ingesting and generating arrays
    |   |                     from various file formats
    │   ├── models/        <- implementations of deep learning models
    │   ├── scrapers/      <- scrapy projects for cataloguing and saving raster datasets
    |   |                     from the internet
    │   └── tools/         <- external utility submodules
    |
    ├── data/              <- Data directory
    │   ├── raw/           <- raw data. should not be modified.
    │   ├── intermediate/  <- suggested place to store partially processed data
    │   ├── processed/     <- suggsted place to store processed training data
    │   └── volumes/       <- suggested place to mount large data volumes
    |
    ├── docs/              <- Sphinx documentation. run sphinx-apidoc here to produce HTML
    |
    ├── envs/              <- conda environment specifications
    |
    ├── figures/            <- Figures saved by scripts or notebooks.
    │
    ├── INFER/            <- Raw imagery to be used for inference
    │
    ├── MODELS/            <- Trained model checkpoints
    │
    ├── notebooks/          <- Jupyter notebooks with user-friendly interfaces to run training and inference.
    │   └── Portal/        <- Demos of package features and common workflows in notebooks
    │
    ├── output/             <- Suggested place to store any local outputs
    |
    ├── PREDICTIONS/         <- Inference outputs
    │
    ├── references/         <- Resources for studying EO-related object detection machine-learning and project installation
    │
    ├── tests/              <- Unit tests.
    |
    ├── TRAIN/        <- Storage location for raw training data
    │   ├── masks/    <- suggested place to store training masks corresponding to rasters stored
    │   ├── rasters/  <- suggested place to store training rasters
    │
    ├── Dockerfile          <- Instructions to build docker container
    |
    ├── docker-compose.yml  <- Configuration with which to launch docker container
    │
    ├── Makefile            <- Makefile with commands like `make environment` 
    │
    ├── README.md           <- The top-level README for developers using this project.


--------


Set up
------------

Please follow the instructions in see  [index](./docs/build/html/index.html) for installation steps, the framework's features and components.  You can also check [setup](./references/SETUP.md) for further details on setting up the framework.

