��}�      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Installation�h]�h	�Text����Installation�����}�(hh�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�IC:\Users\Tresor\Documents\Gitlab\projects\cv_pack\docs\source\install.rst�hKubh)��}�(hhh]�(h)��}�(h�Overview�h]�h�Overview�����}�(hh2hh0hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh-hhhh,hKubh	�	paragraph���)��}�(hX  The codebase takes the form of a python library, ``gim_cv``, embedded in a docker
container together with various scripts (and jupyter notebooks) which cover common
use cases such as training a segmentation model and running inference with a trained
model to create segmented rasters.�h]�(h�1The codebase takes the form of a python library, �����}�(h�1The codebase takes the form of a python library, �hh@hhhNhNubh	�literal���)��}�(h�
``gim_cv``�h]�h�gim_cv�����}�(hhhhKhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhh@ubh��, embedded in a docker
container together with various scripts (and jupyter notebooks) which cover common
use cases such as training a segmentation model and running inference with a trained
model to create segmented rasters.�����}�(h��, embedded in a docker
container together with various scripts (and jupyter notebooks) which cover common
use cases such as training a segmentation model and running inference with a trained
model to create segmented rasters.�hh@hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKhh-hhubh?)��}�(h��Installation boils down to correctly configuring the ``docker-compose.yml`` file to
perform any mapping of volumes to be used (where data and models live), then
spinning up the docker container and entering the anaconda environment therein.�h]�(h�5Installation boils down to correctly configuring the �����}�(h�5Installation boils down to correctly configuring the �hhdhhhNhNubhJ)��}�(h�``docker-compose.yml``�h]�h�docker-compose.yml�����}�(hhhhmhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhhdubh�� file to
perform any mapping of volumes to be used (where data and models live), then
spinning up the docker container and entering the anaconda environment therein.�����}�(h�� file to
perform any mapping of volumes to be used (where data and models live), then
spinning up the docker container and entering the anaconda environment therein.�hhdhhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKhh-hhubh?)��}�(hX  The source code on the host machine is mirrored into the container environment, so that
you may edit the source in either (for example, in a text editor on your host machine,
or when attached to a remote jupyter notebook session running in the container itself).�h]�hX  The source code on the host machine is mirrored into the container environment, so that
you may edit the source in either (for example, in a text editor on your host machine,
or when attached to a remote jupyter notebook session running in the container itself).�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKhh-hhubh?)��}�(h�uThe codebase has been developed to run on a Linux OS, and possibly (hopefully!) on
WSL v2 with nvidia/docker support.�h]�h�uThe codebase has been developed to run on a Linux OS, and possibly (hopefully!) on
WSL v2 with nvidia/docker support.�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKhh-hhubeh}�(h!]��overview�ah#]�h%]��overview�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h� Docker installation instructions�h]�h� Docker installation instructions�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hKubh?)��}�(h��Prerequisites are to install `docker`_ and `nvidia-docker`_ (to allow use of the GPU in
the container). Follow the instructions at these links first and test that they work
according to the documentation.�h]�(h�Prerequisites are to install �����}�(h�Prerequisites are to install �hh�hhhNhNubh	�	reference���)��}�(h�	`docker`_�h]�h�docker�����}�(h�docker�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�hΌrefuri��Chttps://docs.docker.com/engine/install/ubuntu/#installation-methods�uh+h�hh��resolved�Kubh� and �����}�(h� and �hh�hhhNhNubh�)��}�(h�`nvidia-docker`_�h]�h�nvidia-docker�����}�(h�nvidia-docker�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�h�h֌Thttps://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html�uh+h�hh�h�Kubh�� (to allow use of the GPU in
the container). Follow the instructions at these links first and test that they work
according to the documentation.�����}�(h�� (to allow use of the GPU in
the container). Follow the instructions at these links first and test that they work
according to the documentation.�hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKhh�hhubh	�target���)��}�(h�O.. _docker: https://docs.docker.com/engine/install/ubuntu/#installation-methods�h]�h}�(h!]��docker�ah#]�h%]��docker�ah']�h)]�h�h�uh+h�hKhh�hhhh,�
referenced�Kubh�)��}�(h�g.. _nvidia-docker: https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html�h]�h}�(h!]��nvidia-docker�ah#]�h%]��nvidia-docker�ah']�h)]�h�h�uh+h�hKhh�hhhh,j  Kubh)��}�(hhh]�(h)��}�(h�!Setting NVIDIA runtime as default�h]�h�!Setting NVIDIA runtime as default�����}�(hj  hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj  hhhh,hK"ubh?)��}�(h��To ensure that your docker containers can access the GPU by default, edit/create the file
``/etc/docker/daemon.json`` so that it looks like this::�h]�(h�ZTo ensure that your docker containers can access the GPU by default, edit/create the file
�����}�(h�ZTo ensure that your docker containers can access the GPU by default, edit/create the file
�hj&  hhhNhNubhJ)��}�(h�``/etc/docker/daemon.json``�h]�h�/etc/docker/daemon.json�����}�(hhhj/  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj&  ubh� so that it looks like this:�����}�(h� so that it looks like this:�hj&  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK$hj  hhubh	�literal_block���)��}�(h��{
    "default-runtime": "nvidia",
    "runtimes": {
        "nvidia": {
            "path": "nvidia-container-runtime",
            "runtimeArgs": []
        }
    }
}�h]�h��{
    "default-runtime": "nvidia",
    "runtimes": {
        "nvidia": {
            "path": "nvidia-container-runtime",
            "runtimeArgs": []
        }
    }
}�����}�(hhhjJ  ubah}�(h!]�h#]�h%]�h']�h)]��	xml:space��preserve�uh+jH  hh,hK'hj  hhubh?)��}�(h�}If this is the first time you edit this file on your machine, restart the docker
service now to make sure this takes effect::�h]�h�|If this is the first time you edit this file on your machine, restart the docker
service now to make sure this takes effect:�����}�(h�|If this is the first time you edit this file on your machine, restart the docker
service now to make sure this takes effect:�hjZ  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK1hj  hhubjI  )��}�(h�'$ sudo systemctl restart docker.service�h]�h�'$ sudo systemctl restart docker.service�����}�(hhhji  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hK4hj  hhubeh}�(h!]��!setting-nvidia-runtime-as-default�ah#]�h%]��!setting nvidia runtime as default�ah']�h)]�uh+h
hh�hhhh,hK"ubh)��}�(hhh]�(h)��}�(h�Linking disk volumes�h]�h�Linking disk volumes�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj  hhhh,hK7ubh?)��}�(hXe  Before creating the docker container, first check the ``volumes`` entry of ``docker-compose.yml``.
If you intend to use/create disk-space-heavy resources (usually the case!) you will want to
make sure you have the appropriate disk volumes linked to the container environment. Typically
these will be volumes used for storing datasets and for storing models.�h]�(h�6Before creating the docker container, first check the �����}�(h�6Before creating the docker container, first check the �hj�  hhhNhNubhJ)��}�(h�``volumes``�h]�h�volumes�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh�
 entry of �����}�(h�
 entry of �hj�  hhhNhNubhJ)��}�(h�``docker-compose.yml``�h]�h�docker-compose.yml�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubhX  .
If you intend to use/create disk-space-heavy resources (usually the case!) you will want to
make sure you have the appropriate disk volumes linked to the container environment. Typically
these will be volumes used for storing datasets and for storing models.�����}�(hX  .
If you intend to use/create disk-space-heavy resources (usually the case!) you will want to
make sure you have the appropriate disk volumes linked to the container environment. Typically
these will be volumes used for storing datasets and for storing models.�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK9hj  hhubh?)��}�(h��Suppose you have two disks (or EBS volumes).
Let's assume these correspond to ``models`` and ``datasets``. These might also be
different directories of the same drive. You can look at the drive device paths by running
``lsblk`` on the command line.�h]�(h�PSuppose you have two disks (or EBS volumes).
Let’s assume these correspond to �����}�(h�NSuppose you have two disks (or EBS volumes).
Let's assume these correspond to �hj�  hhhNhNubhJ)��}�(h�
``models``�h]�h�models�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh� and �����}�(h� and �hj�  hhhNhNubhJ)��}�(h�``datasets``�h]�h�datasets�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh�q. These might also be
different directories of the same drive. You can look at the drive device paths by running
�����}�(h�q. These might also be
different directories of the same drive. You can look at the drive device paths by running
�hj�  hhhNhNubhJ)��}�(h�	``lsblk``�h]�h�lsblk�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh� on the command line.�����}�(h� on the command line.�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK>hj  hhubh?)��}�(h��The default directories where datasets and models are looked for (according to ``config.yml``) are
currently ``/gim-cv/data/volumes/datasets`` and ``/gim-cv/saved_models/ebs_trained_models``.�h]�(h�OThe default directories where datasets and models are looked for (according to �����}�(h�OThe default directories where datasets and models are looked for (according to �hj  hhhNhNubhJ)��}�(h�``config.yml``�h]�h�
config.yml�����}�(hhhj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj  ubh�) are
currently �����}�(h�) are
currently �hj  hhhNhNubhJ)��}�(h�!``/gim-cv/data/volumes/datasets``�h]�h�/gim-cv/data/volumes/datasets�����}�(hhhj)  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj  ubh� and �����}�(h� and �hj  hhhNhNubhJ)��}�(h�+``/gim-cv/saved_models/ebs_trained_models``�h]�h�'/gim-cv/saved_models/ebs_trained_models�����}�(hhhj<  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj  ubh�.�����}�(h�.�hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKChj  hhubh?)��}�(h�5There are two different ways to mirroring the drives:�h]�h�5There are two different ways to mirroring the drives:�����}�(hjW  hjU  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKFhj  hhubh	�bullet_list���)��}�(hhh]�(h	�	list_item���)��}�(hX�  Mount the volumes at the appropriate locations within the project repository
before launching the container. Say the relevant drive partitions are at
``/dev/nvme1n1p1`` for datasets and ``/dev/nvme2n1p1`` for models.
The binding will then be taken care of by the ``.:/home/root`` directive in ``docker-compose.yml``::

   $ sudo mount /dev/nvme1n1p1 /path/to/gim_cv/data/volumes/datasets
   $ sudo mount /dev/nvme2n1p1 /path/to/gim_cv/saved_models/ebs_trained_models
�h]�(h?)��}�(hX=  Mount the volumes at the appropriate locations within the project repository
before launching the container. Say the relevant drive partitions are at
``/dev/nvme1n1p1`` for datasets and ``/dev/nvme2n1p1`` for models.
The binding will then be taken care of by the ``.:/home/root`` directive in ``docker-compose.yml``::�h]�(h��Mount the volumes at the appropriate locations within the project repository
before launching the container. Say the relevant drive partitions are at
�����}�(h��Mount the volumes at the appropriate locations within the project repository
before launching the container. Say the relevant drive partitions are at
�hjn  hhhNhNubhJ)��}�(h�``/dev/nvme1n1p1``�h]�h�/dev/nvme1n1p1�����}�(hhhjw  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhjn  ubh� for datasets and �����}�(h� for datasets and �hjn  hhhNhNubhJ)��}�(h�``/dev/nvme2n1p1``�h]�h�/dev/nvme2n1p1�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhjn  ubh�; for models.
The binding will then be taken care of by the �����}�(h�; for models.
The binding will then be taken care of by the �hjn  hhhNhNubhJ)��}�(h�``.:/home/root``�h]�h�.:/home/root�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhjn  ubh� directive in �����}�(h� directive in �hjn  hhhNhNubhJ)��}�(h�``docker-compose.yml``�h]�h�docker-compose.yml�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhjn  ubh�:�����}�(h�:�hjn  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKHhjj  ubjI  )��}�(h��$ sudo mount /dev/nvme1n1p1 /path/to/gim_cv/data/volumes/datasets
$ sudo mount /dev/nvme2n1p1 /path/to/gim_cv/saved_models/ebs_trained_models�h]�h��$ sudo mount /dev/nvme1n1p1 /path/to/gim_cv/data/volumes/datasets
$ sudo mount /dev/nvme2n1p1 /path/to/gim_cv/saved_models/ebs_trained_models�����}�(hhhj�  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hKMhjj  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+jh  hje  hhhh,hNubji  )��}�(hXy  Mount the volume first on the host machine at, say, ``/mnt/bigdata/``. Now map the volumes
directly in ``docker-compose.yml``. You will want to add to the ``volumes`` entry
something like::

   - type: bind
     source: /mnt/models
     target: /home/root/saved_models/ebs_trained_models
   - type: bind
     source: /mnt/datasets
     target: /home/root/data/volumes/datasets
�h]�(h?)��}�(h��Mount the volume first on the host machine at, say, ``/mnt/bigdata/``. Now map the volumes
directly in ``docker-compose.yml``. You will want to add to the ``volumes`` entry
something like::�h]�(h�4Mount the volume first on the host machine at, say, �����}�(h�4Mount the volume first on the host machine at, say, �hj�  hhhNhNubhJ)��}�(h�``/mnt/bigdata/``�h]�h�/mnt/bigdata/�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh�". Now map the volumes
directly in �����}�(h�". Now map the volumes
directly in �hj�  hhhNhNubhJ)��}�(h�``docker-compose.yml``�h]�h�docker-compose.yml�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh�. You will want to add to the �����}�(h�. You will want to add to the �hj�  hhhNhNubhJ)��}�(h�``volumes``�h]�h�volumes�����}�(hhhj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh� entry
something like:�����}�(h� entry
something like:�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKPhj�  ubjI  )��}�(h��- type: bind
  source: /mnt/models
  target: /home/root/saved_models/ebs_trained_models
- type: bind
  source: /mnt/datasets
  target: /home/root/data/volumes/datasets�h]�h��- type: bind
  source: /mnt/models
  target: /home/root/saved_models/ebs_trained_models
- type: bind
  source: /mnt/datasets
  target: /home/root/data/volumes/datasets�����}�(hhhj)  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hKThj�  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+jh  hje  hhhh,hNubeh}�(h!]�h#]�h%]�h']�h)]��bullet��*�uh+jc  hh,hKHhj  hhubh?)��}�(h�UThese default directories can be changed as you see fit through :ref:`configuration`.�h]�(h�@These default directories can be changed as you see fit through �����}�(h�@These default directories can be changed as you see fit through �hjE  hhhNhNubh �pending_xref���)��}�(h�:ref:`configuration`�h]�h	�inline���)��}�(hjR  h]�h�configuration�����}�(hhhjV  hhhNhNubah}�(h!]�h#]�(�xref��std��std-ref�eh%]�h']�h)]�uh+jT  hjP  ubah}�(h!]�h#]�h%]�h']�h)]��refdoc��install��	refdomain�ja  �reftype��ref��refexplicit���refwarn���	reftarget��configuration�uh+jN  hh,hK[hjE  ubh�.�����}�(hjN  hjE  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK[hj  hhubh?)��}�(h��Once the container is running (see the following section) you should check that the files
you are expecting to be there are appearing where you have mapped them. For example::�h]�h��Once the container is running (see the following section) you should check that the files
you are expecting to be there are appearing where you have mapped them. For example:�����}�(h��Once the container is running (see the following section) you should check that the files
you are expecting to be there are appearing where you have mapped them. For example:�hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK]hj  hhubjI  )��}�(h�X$ docker exec -ti gim_cv_container /bin/bash
$ ls /home/root/saved_models/bigdata_models�h]�h�X$ docker exec -ti gim_cv_container /bin/bash
$ ls /home/root/saved_models/bigdata_models�����}�(hhhj�  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hK`hj  hhubh?)��}�(h�8If these are not present, you have done something wrong.�h]�h�8If these are not present, you have done something wrong.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKchj  hhubh?)��}�(hX	  *Bear in mind when working across multiple machines that these data drives may follow a different
structure. It makes sense then to add ``docker-compose.yml`` to ``.gitignore`` and maintain separate
versions (differing in the ``volumes`` section) for each machine.*�h]�h	�emphasis���)��}�(hj�  h]�hX  Bear in mind when working across multiple machines that these data drives may follow a different
structure. It makes sense then to add ``docker-compose.yml`` to ``.gitignore`` and maintain separate
versions (differing in the ``volumes`` section) for each machine.�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKehj  hhubeh}�(h!]��linking-disk-volumes�ah#]�h%]��linking disk volumes�ah']�h)]�uh+h
hh�hhhh,hK7ubh)��}�(hhh]�(h)��}�(h�7Build docker image containing the code and dependencies�h]�h�7Build docker image containing the code and dependencies�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hKkubh?)��}�(hX#  Now use ``docker-compose`` to build the image specified in ``Dockerfile``. This creates an
image called ``gim_cv``, installs geospatial libraries like GDAL on the OS image, installs
Anaconda, copies the source code of this repository into the image and finally builds the
python enviornment.�h]�(h�Now use �����}�(h�Now use �hj�  hhhNhNubhJ)��}�(h�``docker-compose``�h]�h�docker-compose�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh�! to build the image specified in �����}�(h�! to build the image specified in �hj�  hhhNhNubhJ)��}�(h�``Dockerfile``�h]�h�
Dockerfile�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh�. This creates an
image called �����}�(h�. This creates an
image called �hj�  hhhNhNubhJ)��}�(h�
``gim_cv``�h]�h�gim_cv�����}�(hhhj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh��, installs geospatial libraries like GDAL on the OS image, installs
Anaconda, copies the source code of this repository into the image and finally builds the
python enviornment.�����}�(h��, installs geospatial libraries like GDAL on the OS image, installs
Anaconda, copies the source code of this repository into the image and finally builds the
python enviornment.�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKmhj�  hhubh?)��}�(h�1From the root of the ``gim_cv`` repository, run::�h]�(h�From the root of the �����}�(h�From the root of the �hj$  hhhNhNubhJ)��}�(h�
``gim_cv``�h]�h�gim_cv�����}�(hhhj-  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj$  ubh� repository, run:�����}�(h� repository, run:�hj$  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKrhj�  hhubjI  )��}�(h�docker-compose up -d�h]�h�docker-compose up -d�����}�(hhhjF  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hKthj�  hhubh?)��}�(h�eIf you run into an error like ``Couldn't connect to docker daemon``, do the
following and try again::�h]�(h�If you run into an error like �����}�(h�If you run into an error like �hjT  hhhNhNubhJ)��}�(h�%``Couldn't connect to docker daemon``�h]�h�!Couldn't connect to docker daemon�����}�(hhhj]  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhjT  ubh�!, do the
following and try again:�����}�(h�!, do the
following and try again:�hjT  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKwhj�  hhubjI  )��}�(h�8sudo systemctl unmask docker
sudo systemctl start docker�h]�h�8sudo systemctl unmask docker
sudo systemctl start docker�����}�(hhhjv  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hKzhj�  hhubeh}�(h!]��7build-docker-image-containing-the-code-and-dependencies�ah#]�h%]��7build docker image containing the code and dependencies�ah']�h)]�uh+h
hh�hhhh,hKkubh)��}�(hhh]�(h)��}�(h�/Create a container from the image and launch it�h]�h�/Create a container from the image and launch it�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hKubh?)��}�(h��Once you've run ``docker-compose up -d``, a specific instance of this image
(a container) called ``gim_cv_container`` is created, with the specifics configured
as in ``docker-compose.yml`` (port-forwarding, volume mirroring with the host OS etc.).�h]�(h�Once you’ve run �����}�(h�Once you've run �hj�  hhhNhNubhJ)��}�(h�``docker-compose up -d``�h]�h�docker-compose up -d�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh�9, a specific instance of this image
(a container) called �����}�(h�9, a specific instance of this image
(a container) called �hj�  hhhNhNubhJ)��}�(h�``gim_cv_container``�h]�h�gim_cv_container�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh�1 is created, with the specifics configured
as in �����}�(h�1 is created, with the specifics configured
as in �hj�  hhhNhNubhJ)��}�(h�``docker-compose.yml``�h]�h�docker-compose.yml�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh�; (port-forwarding, volume mirroring with the host OS etc.).�����}�(h�; (port-forwarding, volume mirroring with the host OS etc.).�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj�  hhubh?)��}�(h�jCheck that your container is up and running by running ``docker ps``. You should see
something like this::�h]�(h�7Check that your container is up and running by running �����}�(h�7Check that your container is up and running by running �hj�  hhhNhNubhJ)��}�(h�``docker ps``�h]�h�	docker ps�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh�%. You should see
something like this:�����}�(h�%. You should see
something like this:�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj�  hhubjI  )��}�(hX  CONTAINER ID        IMAGE               COMMAND               CREATED             STATUS              PORTS                    NAMES
44315329363a        gim_cv        "tail -f /dev/null"   29 seconds ago      Up 19 seconds       0.0.0.0:8888->8888/tcp   gim_cv_container�h]�hX  CONTAINER ID        IMAGE               COMMAND               CREATED             STATUS              PORTS                    NAMES
44315329363a        gim_cv        "tail -f /dev/null"   29 seconds ago      Up 19 seconds       0.0.0.0:8888->8888/tcp   gim_cv_container�����}�(hhhj  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hK�hj�  hhubeh}�(h!]��/create-a-container-from-the-image-and-launch-it�ah#]�h%]��/create a container from the image and launch it�ah']�h)]�uh+h
hh�hhhh,hKubh)��}�(hhh]�(h)��}�(h�;Optional - disable firewall (in case of problems on CentOS)�h]�h�;Optional - disable firewall (in case of problems on CentOS)�����}�(hj"  hj   hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj  hhhh,hK�ubh?)��}�(h�eIf, on Linux, you run into the issue that the containers have no internet
(eg apt update fails), do::�h]�h�dIf, on Linux, you run into the issue that the containers have no internet
(eg apt update fails), do:�����}�(h�dIf, on Linux, you run into the issue that the containers have no internet
(eg apt update fails), do:�hj.  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj  hhubjI  )��}�(h�S$ sudo systemctl disable firewalld
$ systemctl stop docker
$ systemctl start docker�h]�h�S$ sudo systemctl disable firewalld
$ systemctl stop docker
$ systemctl start docker�����}�(hhhj=  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hK�hj  hhubeh}�(h!]��7optional-disable-firewall-in-case-of-problems-on-centos�ah#]�h%]��;optional - disable firewall (in case of problems on centos)�ah']�h)]�uh+h
hh�hhhh,hK�ubh)��}�(hhh]�(h)��}�(h�Attach to the container�h]�h�Attach to the container�����}�(hjX  hjV  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhjS  hhhh,hK�ubh?)��}�(h��Now attach to the container to run code in the environment. It's recommended to do this
in a `tmux`_ session so that you can run things in the background by detaching
if you desire::�h]�(h�_Now attach to the container to run code in the environment. It’s recommended to do this
in a �����}�(h�]Now attach to the container to run code in the environment. It's recommended to do this
in a �hjd  hhhNhNubh�)��}�(h�`tmux`_�h]�h�tmux�����}�(h�tmux�hjm  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�ju  h֌4https://linuxize.com/post/getting-started-with-tmux/�uh+h�hjd  h�Kubh�Q session so that you can run things in the background by detaching
if you desire:�����}�(h�Q session so that you can run things in the background by detaching
if you desire:�hjd  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hjS  hhubjI  )��}�(h�,$ docker exec -it gim_cv_container /bin/bash�h]�h�,$ docker exec -it gim_cv_container /bin/bash�����}�(hhhj�  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hK�hjS  hhubh�)��}�(h�?.. _tmux : https://linuxize.com/post/getting-started-with-tmux/�h]�h}�(h!]��tmux�ah#]�h%]��tmux�ah']�h)]�h�j}  uh+h�hK�hjS  hhhh,j  Kubh?)��}�(h�&Finally enter the python environment::�h]�h�%Finally enter the python environment:�����}�(h�%Finally enter the python environment:�hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hjS  hhubjI  )��}�(h�$ conda activate gim_cv_gpu�h]�h�$ conda activate gim_cv_gpu�����}�(hhhj�  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hK�hjS  hhubh?)��}�(h�Hand install ``gim_cv`` as a local python package in editable mode with::�h]�(h�and install �����}�(h�and install �hj�  hhhNhNubhJ)��}�(h�
``gim_cv``�h]�h�gim_cv�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh�1 as a local python package in editable mode with:�����}�(h�1 as a local python package in editable mode with:�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hjS  hhubjI  )��}�(h�$ pip install -e .�h]�h�$ pip install -e .�����}�(hhhj�  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hK�hjS  hhubh?)��}�(h�$Press ctrl+D or type 'exit' to exit.�h]�h�(Press ctrl+D or type ‘exit’ to exit.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hjS  hhubh?)��}�(h��Note that if you shut down the container with ``docker-compose down``, you will have
to repeat this last step of installing the local package the next time you recreate it.�h]�(h�.Note that if you shut down the container with �����}�(h�.Note that if you shut down the container with �hj�  hhhNhNubhJ)��}�(h�``docker-compose down``�h]�h�docker-compose down�����}�(hhhj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh�g, you will have
to repeat this last step of installing the local package the next time you recreate it.�����}�(h�g, you will have
to repeat this last step of installing the local package the next time you recreate it.�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hjS  hhubeh}�(h!]��attach-to-the-container�ah#]�h%]��attach to the container�ah']�h)]�uh+h
hh�hhhh,hK�ubeh}�(h!]�� docker-installation-instructions�ah#]�h%]�� docker installation instructions�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�Testing your installation�h]�h�Testing your installation�����}�(hj5  hj3  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj0  hhhh,hK�ubh?)��}�(h�EHere are some quick checks you can do to see that things are working.�h]�h�EHere are some quick checks you can do to see that things are working.�����}�(hjC  hjA  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj0  hhubh	�block_quote���)��}�(hhh]�jd  )��}�(hhh]�(ji  )��}�(h�~Check that the GPU is accessible from the python interpreter::

  import tensorflow as tf
  assert tf.test.is_gpu_available()
�h]�(h?)��}�(h�>Check that the GPU is accessible from the python interpreter::�h]�h�=Check that the GPU is accessible from the python interpreter:�����}�(h�=Check that the GPU is accessible from the python interpreter:�hj[  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hjW  ubjI  )��}�(h�9import tensorflow as tf
assert tf.test.is_gpu_available()�h]�h�9import tensorflow as tf
assert tf.test.is_gpu_available()�����}�(hhhjj  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hK�hjW  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+jh  hjT  ubji  )��}�(h�,Import the gim_cv module::

  import gim_cv
�h]�(h?)��}�(h�Import the gim_cv module::�h]�h�Import the gim_cv module:�����}�(h�Import the gim_cv module:�hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj~  ubjI  )��}�(h�import gim_cv�h]�h�import gim_cv�����}�(hhhj�  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hK�hj~  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+jh  hjT  ubji  )��}�(h��Check that any mountpoints contain the expected files (after you have done
:ref:`configuration`)::

  from pathlib import Path
  import gim_cv.config as cfg

  print(list(Path(cfg.models_path).glob('*')))
�h]�(h?)��}�(h�bCheck that any mountpoints contain the expected files (after you have done
:ref:`configuration`)::�h]�(h�KCheck that any mountpoints contain the expected files (after you have done
�����}�(h�KCheck that any mountpoints contain the expected files (after you have done
�hj�  hhhNhNubjO  )��}�(h�:ref:`configuration`�h]�jU  )��}�(hj�  h]�h�configuration�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�(j`  �std��std-ref�eh%]�h']�h)]�uh+jT  hj�  ubah}�(h!]�h#]�h%]�h']�h)]��refdoc�jm  �	refdomain�j�  �reftype��ref��refexplicit���refwarn��js  �configuration�uh+jN  hh,hK�hj�  ubh�):�����}�(h�):�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj�  ubjI  )��}�(h�bfrom pathlib import Path
import gim_cv.config as cfg

print(list(Path(cfg.models_path).glob('*')))�h]�h�bfrom pathlib import Path
import gim_cv.config as cfg

print(list(Path(cfg.models_path).glob('*')))�����}�(hhhj�  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hK�hj�  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+jh  hjT  ubji  )��}�(h�VRun pytest and check that the tests pass. In ``/home/root/``, run::

  $ pytest tests
�h]�(h?)��}�(h�CRun pytest and check that the tests pass. In ``/home/root/``, run::�h]�(h�-Run pytest and check that the tests pass. In �����}�(h�-Run pytest and check that the tests pass. In �hj�  hhhNhNubhJ)��}�(h�``/home/root/``�h]�h�/home/root/�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhj�  ubh�, run:�����}�(h�, run:�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj�  ubjI  )��}�(h�$ pytest tests�h]�h�$ pytest tests�����}�(hhhj  ubah}�(h!]�h#]�h%]�h']�h)]�jX  jY  uh+jH  hh,hK�hj�  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+jh  hjT  ubeh}�(h!]�h#]�h%]�h']�h)]�jC  jD  uh+jc  hh,hK�hjQ  ubah}�(h!]�h#]�h%]�h']�h)]�uh+jO  hj0  hhhNhNubeh}�(h!]��testing-your-installation�ah#]�h%]��testing your installation�ah']�h)]�uh+h
hhhhhh,hK�ubh)��}�(hhh]�(h)��}�(h�Notes�h]�h�Notes�����}�(hjD  hjB  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj?  hhhh,hK�ubh?)��}�(hXQ  The ``docker-compose.yml`` file also creates a `Splash`_ container which
runs a splash server in the background. This is useful for webscraping
interactive websites (as is done in :py:mod:`gim_cv.scrapers.vl_orthos`
for obtaining Flemish orthophoto download links). This has not been used
in a while and most likely you will not need it.�h]�(h�The �����}�(h�The �hjP  hhhNhNubhJ)��}�(h�``docker-compose.yml``�h]�h�docker-compose.yml�����}�(hhhjY  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hIhjP  ubh� file also creates a �����}�(h� file also creates a �hjP  hhhNhNubh�)��}�(h�	`Splash`_�h]�h�Splash�����}�(h�Splash�hjl  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�jt  h֌0https://splash.readthedocs.io/en/stable/api.html�uh+h�hjP  h�Kubh�| container which
runs a splash server in the background. This is useful for webscraping
interactive websites (as is done in �����}�(h�| container which
runs a splash server in the background. This is useful for webscraping
interactive websites (as is done in �hjP  hhhNhNubjO  )��}�(h�#:py:mod:`gim_cv.scrapers.vl_orthos`�h]�hJ)��}�(hj�  h]�h�gim_cv.scrapers.vl_orthos�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�(j`  �py��py-mod�eh%]�h']�h)]�uh+hIhj�  ubah}�(h!]�h#]�h%]�h']�h)]��refdoc�jm  �	refdomain�j�  �reftype��mod��refexplicit���refwarn���	py:module�N�py:class�Njs  �gim_cv.scrapers.vl_orthos�uh+jN  hh,hK�hjP  ubh�z
for obtaining Flemish orthophoto download links). This has not been used
in a while and most likely you will not need it.�����}�(h�z
for obtaining Flemish orthophoto download links). This has not been used
in a while and most likely you will not need it.�hjP  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj?  hhubh�)��}�(h�<.. _Splash: https://splash.readthedocs.io/en/stable/api.html�h]�h}�(h!]��splash�ah#]�h%]��splash�ah']�h)]�h�j|  uh+h�hK�hj?  hhhh,j  Kubeh}�(h!]��notes�ah#]�h%]��notes�ah']�h)]�uh+h
hhhhhh,hK�ubeh}�(h!]��installation�ah#]�h%]��installation�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�J ���pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}�(�docker�]�h�a�nvidia-docker�]�h�a�tmux�]�jm  a�splash�]�jl  au�refids�}��nameids�}�(j�  j�  h�h�j-  j*  j  j  j  j  j|  jy  j�  j�  j�  j�  j  j  jP  jM  j%  j"  j�  j�  j<  j9  j�  j�  j�  j�  u�	nametypes�}�(j�  Nh�Nj-  Nj  �j  �j|  Nj�  Nj�  Nj  NjP  Nj%  Nj�  �j<  Nj�  Nj�  �uh!}�(j�  hh�h-j*  h�j  h�j  j	  jy  j  j�  j  j�  j�  j  j�  jM  j  j"  jS  j�  j�  j9  j0  j�  j?  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.